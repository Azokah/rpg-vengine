#include "Mapa.h"


Mapa::Mapa(SDL_Renderer * RENDER){
    sprite = new Sprite();
    sprite->agregarFrame(32,0,32,32);
    for(int i = 0; i < MAPA_H; i++)
        for(int j = 0; j < MAPA_W; j++)
            mapa[i][j] = 0;
    
    objetos.push_back(new Arma(30, 35, "Espada de madera.",1,1,CORTANTE));
    objetos.push_back(new Escudo(25,30,"Escudo piola"));
    objetos.push_back(new Armadura(22,26,"Armadura piola"));
    objetos.push_back(new Consumible(28,28,"Pocion piola"));

    for(int i = 20; i < 31; i++){
        for(int j = 20; j < 31; j++){
            if(i%10 == 0 || j%10 == 0) estructuras.push_back(new Estructura("Casa",i,j, 11));
            if(i%10 == 5 && j%10 == 0) estructuras.push_back(new Estructura("Puerta",i,j,1));
        }
    }
    
};
Mapa::~Mapa(){};

int Mapa::getMapa(int x, int y){return mapa[x][y];};
