#include "PersonajeJ.h"

PersonajeJ::PersonajeJ(std::string NOMBRE, int X, int Y) : Personaje(NOMBRE,X,Y), Camina(X,Y){
};

PersonajeJ::~PersonajeJ(){};


void PersonajeJ::input(int tecla, bool estado)
{
    switch (tecla)
    {
    case SDL_SCANCODE_ESCAPE:
        if (estado)
            std::cout << nombre << ": Queres salir picaron?" << std::endl;
        break;
    }
};

void PersonajeJ::inputMouse(int tecla, int X, int Y)
{
    std::cout << "Click con: " << tecla << " en: " << X << " - " << Y << std::endl;
    switch (tecla)
    {
    case SDL_BUTTON_RIGHT:
        moveTo(X, Y);
        break;
    }
};

void PersonajeJ::update()
{
    if(move()) sprite->play(direccion);
    else sprite->stop();
    sprite->run();
};