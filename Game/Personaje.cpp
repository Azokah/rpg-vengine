#include "Personaje.h"

Personaje::Personaje(std::string NOMBRE, int X, int Y) : Entidad(NOMBRE){
    free(sprite);
    sprite = new Sprite();
    sprite->agregarFrame(0, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);
    sprite->agregarFrame(32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);
    sprite->agregarFrame(0, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);
    sprite->agregarFrame(64, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);

    sprite->agregarFrame(3*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);
    sprite->agregarFrame(4*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);
    sprite->agregarFrame(3*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);
    sprite->agregarFrame(5*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);

    sprite->agregarFrame(6*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);
    sprite->agregarFrame(7*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);
    sprite->agregarFrame(6*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);
    sprite->agregarFrame(8*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);

    sprite->agregarFrame(9*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
    sprite->agregarFrame(10*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
    sprite->agregarFrame(9*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
    sprite->agregarFrame(11*32, 96, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);

    inventario.push_back(new Arma(0, 0, "Espada de las mil verdades.",25,50,CORTANTE));
}


void Personaje::update()
{   
    sprite->play();
    sprite->run();
};