#pragma once
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <vector>
#include "../Constantes.h"
#include "../Engine/Sprite.h"
#include "PersonajeNJ.h"
#include "Objetos.h"
class Hud {
    public:
        Hud(int, int, Entidad*);//No estoy seguro si es necesario que el hud conozca todas las entidades o:
        ~Hud();

        void update();
        void setCursor(int,int);
        bool getCursor();
        void setCursorOff();

        float getCursorX();
        float getCursorY();

        void setObjetivo(Entidad*);
        void resetObjetivo();

        Sprite * cursorSprite, *hudSprite;
        Entidad * entidad, * entidadObjetivo;;
    private:

        float xCursor, yCursor;
        float xHud, yHud;
        SDL_Rect vPortHud;
        bool cursor;
        

};