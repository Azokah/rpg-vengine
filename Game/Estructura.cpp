#include "Estructura.h"

Estructura::Estructura(std::string NOMBRE,int X, int Y, int V) : Entidad(NOMBRE){
    nombre = NOMBRE;
    x = X;
    y = Y;
    valorEstructura = V;
    direccion = 0;
    free(sprite);
    sprite = new Sprite();
    sprite->agregarFrame(0,0,32,32);

};
Estructura::~Estructura(){};


int Estructura::getValor(){
    return valorEstructura;
}