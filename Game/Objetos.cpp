#include "Objetos.h"


objetosTipo Objetos::getTipo(){
	return tipo;
};

Arma::Arma(int X, int Y,std::string NOMBRE, int MINDMG, int MAXDMG, armasTipo TIPO) : Objetos(NOMBRE){
	tipo = ARMA;
	x = X;
	y = Y;
	aTipo = TIPO;
	minDmg = MINDMG;
	maxDmg = MAXDMG;
	free(sprite);
	sprite = new Sprite();
	sprite->agregarFrame(0,5*32,TILE_W,TILE_H);
};
Arma::~Arma(){};

Escudo::Escudo(int X, int Y,std::string NOMBRE)  : Objetos(NOMBRE){
	tipo = ESCUDO;
	x = X;
	y = Y;
	free(sprite);
	sprite = new Sprite();
	sprite->agregarFrame(1*32,5*32,TILE_W,TILE_H);
};
Escudo::~Escudo(){};

Armadura::Armadura(int X, int Y,std::string NOMBRE) : Objetos(NOMBRE){
	tipo = ARMADURA;
	x = X;
	y = Y;
	free(sprite);
	sprite = new Sprite();
	sprite->agregarFrame(2*32,5*32,TILE_W,TILE_H);
};
Armadura::~Armadura(){};



Ropa::Ropa(int X, int Y,std::string NOMBRE) : Objetos(NOMBRE){
	tipo = ROPA;
	x = X;
	y = Y;
	free(sprite);
	sprite = new Sprite();
	sprite->agregarFrame(3*32,5*32,TILE_W,TILE_H);
};
Ropa::~Ropa(){};

Consumible::Consumible(int X, int Y,std::string NOMBRE) : Objetos(NOMBRE){
	tipo = CONSUMIBLE;
	x = X;
	y = Y;
	free(sprite);
	sprite = new Sprite();
	sprite->agregarFrame(4*32,5*32,TILE_W,TILE_H);
};

Consumible::~Consumible(){};
