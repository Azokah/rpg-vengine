#include "PersonajeNJ.h"

PersonajeNJ::PersonajeNJ(std::string NOMBRE, int X, int Y) : Personaje(NOMBRE,X,Y), IA(X,Y){
    free(sprite);
    sprite = new Sprite();
    sprite->agregarFrame(0, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);
    sprite->agregarFrame(32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);
    sprite->agregarFrame(0, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);
    sprite->agregarFrame(64, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_ESTE);

    sprite->agregarFrame(3*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);
    sprite->agregarFrame(4*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);
    sprite->agregarFrame(3*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);
    sprite->agregarFrame(5*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_SUR);

    sprite->agregarFrame(6*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);
    sprite->agregarFrame(7*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);
    sprite->agregarFrame(6*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);
    sprite->agregarFrame(8*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_OESTE);

    sprite->agregarFrame(9*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
    sprite->agregarFrame(10*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
    sprite->agregarFrame(9*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
    sprite->agregarFrame(11*32, 32, PJ_SPRITE_W, PJ_SPRITE_H,PJ_DIRECCION_NORTE);
}

PersonajeNJ::~PersonajeNJ(){};


void PersonajeNJ::update()
{
    idle();
    if(move()) sprite->play(direccion);
    else sprite->stop();
    sprite->run();
};