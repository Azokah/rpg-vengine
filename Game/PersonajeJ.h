#pragma once
#include <iostream>
#include <vector>
#include "Personaje.h"
#include "Mecanicas/Camina.h"
#include "Objetos.h"

class PersonajeJ : public Personaje, public Camina {
    public:
        PersonajeJ(std::string NOMBRE, int, int);
        ~PersonajeJ();
        void input(int tecla, bool estado);
        void inputMouse(int,int,int);

        //Override
        void update();
    private:
    
};