#pragma once
#include <iostream>
#include <string>
#include "../Constantes.h"
#include "Entidad.h"
#include "Mecanicas/Inamovible.h"
#include "Mecanicas/Agarrable.h"

enum objetosTipo {
	ARMA,
	ESCUDO,
	ARMADURA,
	ROPA,
	CONSUMIBLE
};

enum armasTipo {
	CORTANTE,
	PERFORANTE,
	CONTUNDENTE,
	MAGICO
};

class Objetos : public Entidad, public Inamovible{//, public Agarrable{
	public:
		Objetos(std::string NOMBRE) : Entidad(NOMBRE) {};
		virtual ~Objetos(){};
		
		objetosTipo getTipo();	

	protected:

		objetosTipo tipo;	
};


class Arma : public Objetos{
	public:
		Arma(int X, int Y, std::string, int, int, armasTipo);
		~Arma();

		armasTipo getATipo();

	private:
		int minDmg, maxDmg;

		armasTipo aTipo;
};

class Escudo : public Objetos {
	public:
		Escudo(int X, int Y, std::string);
		~Escudo();
};

class Armadura : public Objetos {
	public:
		Armadura(int X, int Y, std::string);
		~Armadura();
};

class Ropa : public Objetos {
	public:
		Ropa(int X, int Y, std::string);
		~Ropa();
};

class Consumible : public Objetos {
	public:
		Consumible(int X, int y, std::string);
		~Consumible();
};
