#pragma once
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <vector>
#include <cmath>
#include "Entidad.h"
#include "Mecanicas/Inamovible.h"
#include "../Constantes.h"

class Estructura : public Entidad, public Inamovible {
    public:
        Estructura(std::string, int, int, int);
        ~Estructura();

        int getValor();
    private:
        int valorEstructura;
};