#include "Hud.h"

//Esto tambien es medio cochino... pero funciona...
Hud::Hud(int x, int y, Entidad * ent){
    xHud = x;
    yHud = y;
    xCursor = yCursor = 0;
    entidad = ent;
    vPortHud.x = 5;
    vPortHud.y = 5;
    vPortHud.w = TILE_W;
    vPortHud.h = TILE_H;
    cursor = false;
    cursorSprite = new Sprite();
    cursorSprite->agregarFrame(HUD_CURSOR_X,HUD_CURSOR_X,TILE_W,TILE_H);
    hudSprite = new Sprite();
    hudSprite->agregarFrame(HUD_HUD_X, HUD_HUD_Y,HUD_HUD_W,HUD_HUD_H);
};

Hud::~Hud(){}

void Hud::update(){
    if(cursor){
        if(dynamic_cast<PersonajeNJ*>(entidadObjetivo) != NULL)
            setCursor(dynamic_cast<PersonajeNJ*>(entidadObjetivo)->getX(),dynamic_cast<PersonajeNJ*>(entidadObjetivo)->getY()+TILE_H);
        else setCursor(dynamic_cast<Objetos*>(entidadObjetivo)->getX()*TILE_W,dynamic_cast<Objetos*>(entidadObjetivo)->getY()*TILE_H);
    }
};

void Hud::setCursor(int x,int y){
    //x = ((int)x)/32;
    //y = ((int)y)/32;
    //x = ((int)x)*32;
    //y = ((int)y)*32;
    xCursor = x;
    yCursor = y;
};
bool Hud::getCursor(){ return cursor;};
void Hud::setCursorOff(){ cursor = false; resetObjetivo();};

float Hud::getCursorX(){return xCursor;};
float Hud::getCursorY(){return yCursor;};

void Hud::setObjetivo(Entidad* enty){

    entidadObjetivo = enty;
    if(entidadObjetivo != NULL) cursor = true;
    else cursor = false;
}

void Hud::resetObjetivo(){ entidadObjetivo = NULL;};