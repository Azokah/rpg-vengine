#pragma once
#include <iostream>
#include <vector>
#include "Entidad.h"
#include "Mecanicas/Camina.h"
#include "Objetos.h"

class Personaje : public Entidad {
    public:
        Personaje(std::string NOMBRE, int, int);
        virtual ~Personaje(){};

        //Override
        void update();

    protected:

        std::vector<Objetos *> inventario;
};