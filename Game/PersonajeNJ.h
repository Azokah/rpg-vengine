#pragma once
#include <iostream>
#include <vector>
#include "Personaje.h"
#include "Mecanicas/IA.h"


class PersonajeNJ : public IA, public Personaje {
    public:
        PersonajeNJ(std::string, int, int);
        ~PersonajeNJ();

        //Override
        void update();

    private:

};