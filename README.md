# RPG-GameEngine
<html>
<body>
This is a 2D rpg game with roguelike elements. It's intended it to be Singleplayer but local mutiplayer may be added.
Some features are:
  <ul>
  <li> Open World</li>
  <li> Questing System</li>
  <li> Dialog Trees</li>
  <li> Epic and Randomized Items</li>
  <li> Randomized unique-areas</li>
  <li> Customizable characters</li>
  <li> Skills based system for character progression.</li>
  <li> And much more!</li>
  </ul>
  </body>
 </html>
