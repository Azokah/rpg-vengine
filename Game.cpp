#include "Game.h"

Game &Game::getInstance()
{
    static Game instancia;
    return instancia;
};

Game::Game()
{
    estado = INICIANDO;
    //Inicializar elementos engine
    sdl = new SDLManager();
    in = &in->getInstance();
    camara = &camara->getInstance();
    renderComp = &renderComp->getInstance(sdl->getRender());
    colision = new Colision();
    musica = new Musica();
    musica->tocar();
    timer = new Timer();
    timer->start();
    //Inicializar elementos game
    
    pj = new PersonajeJ("Jorge", 1000, 1000);
    pj2 = new PersonajeNJ("Flores", 800, 800);

    
    personajes.push_back(pj2);

    mapa = new Mapa(sdl->getRender());
    hud = new Hud(0,0,pj); // Perfecto.

    /* 
    * Problemas principales:
        La funcion de dibujar el HUD depende de Game.cpp y no de HUD ni de RenderComponent. Esta cochino.
        Usar Dinamic_cast en hud para castear la clase para conseguir los metodos getX y getY esta mal.
            Una entidad deberia poseer los metodos getX y getY, ya que una entidad tiene posicion.
    */
    bindearInput();

    estado = CORRIENDO;
};
Game::~Game(){
};

void Game::run()
{
    std::cout << "El juego ha comenzado..." << std::endl;
    while (estado == CORRIENDO)
    {
        sdl->limpiarRender();
        update();
        dibujar();
        sdl->renderizar();
    }
};
void Game::update()
{
    in->run(camara);
    pj->update();
    pj2->update();
    hud->update();
    camara->update(pj->getX(), pj->getY());
};
void Game::dibujar(){
    //Objetos de piso y mapa
    for (int i = 0; i < MAPA_H; i++){
        for (int j = 0; j < MAPA_W; j++){
            renderComp->renderizar(j * TILE_W, i * TILE_H, TILE_W, TILE_H, mapa->sprite->getFrame(), camara);
    
            //Dibujar objetos del mapa
            for(int o = 0; o < mapa->objetos.size(); o++){
                    if(mapa->objetos.at(o)->getX() == j && mapa->objetos.at(o)->getY() == i)
                    renderComp->renderizar(j * TILE_W, i * TILE_H, TILE_W, TILE_H, mapa->objetos.at(o)->sprite->getFrame(), camara);
                }
                for(int o = 0; o < mapa->estructuras.size(); o++){
                    if(mapa->estructuras.at(o)->getX() == j && mapa->estructuras.at(o)->getY() == i)
                    renderComp->renderizar(j * TILE_W, i * TILE_H, TILE_W, TILE_H, mapa->estructuras.at(o)->sprite->getFrame(), camara);
                }
            } 
        }
        //Objetos superiores
        for (int i = 0; i < MAPA_H; i++){
            for (int j = 0; j < MAPA_W; j++){
                if((int) pj->getX()/32 == j && (int) pj->getY()/32 == i) renderComp->renderizar(pj->getX(), pj->getY(), PJ_SPRITE_W, PJ_SPRITE_H, pj->sprite->getFrameDir(), camara);
                if((int) pj2->getX()/32 == j && (int) pj2->getY()/32 == i) renderComp->renderizar(pj2->getX(), pj2->getY(), PJ_SPRITE_W, PJ_SPRITE_H, pj2->sprite->getFrameDir(), camara);
            }
        }
    
    dibujarTop();
    

    /*
    if(colision->detectar(pj->getX(),pj->getY(),PJ_SPRITE_W,PJ_SPRITE_H,
        pj2->getX(),pj2->getY(),PJ_SPRITE_W,PJ_SPRITE_H))
            sdl->imprimirTexto("Colision entre Pj1 y pj2.",500,300,255,125,0); //Prueba...
    */
};
void Game::dibujarTop(){
    sdl->drawFPS();
    dibujarHUD();
}
void Game::dibujarHUD(){
    //Esto es horrible y me da asco y lo voy a tener que cambiar o hacer algo... pero funciona
    /* Lo que hace es dibujar el hud(Los dos paneles) y tambien dibujar los items ( el de objetivo y el propio) */
    renderComp->renderizarFixed(HUD_X,HUD_Y,HUD_W,HUD_H,hud->hudSprite->getFrame(),false);
    sdl->imprimirTexto(hud->entidad->getNombre(),HUD_ITEM_TEXT_X,HUD_ITEM_TEXT_Y,75,125,255);
    SDL_Rect clip;
    clip.x = HUD_ITEM_CLIP_X;
    clip.y = HUD_ITEM_CLIP_Y;
    clip.w = HUD_ITEM_CLIP_W;
    clip.h = HUD_ITEM_CLIP_H;
    renderComp->setClip(&clip);
    renderComp->renderizarFixed(HUD_ITEM_X,HUD_ITEM_Y,PJ_SPRITE_W*3,PJ_SPRITE_H*3,hud->entidad->sprite->getFrame(),false);
    renderComp->setClip(NULL);
    if(hud->getCursor()) {
        renderComp->renderizar(hud->getCursorX(),hud->getCursorY(),TILE_W,TILE_H,hud->cursorSprite->getFrame(),camara);
        renderComp->renderizarFixed(HUD_OBJETIVO_X,HUD_OBJETIVO_Y,HUD_W,HUD_H,hud->hudSprite->getFrame(),true);
        sdl->imprimirTexto(hud->entidadObjetivo->getNombre(),HUD_OBJETIVO_ITEM_TEXT_X,HUD_OBJETIVO_ITEM_TEXT_Y,75,125,255);
        clip.x = HUD_OBJETIVO_ITEM_CLIP_X;
        clip.y = HUD_OBJETIVO_ITEM_CLIP_Y;
        clip.w = HUD_OBJETIVO_ITEM_CLIP_W;
        clip.h = HUD_OBJETIVO_ITEM_CLIP_H;
        renderComp->setClip(&clip);
        renderComp->renderizarFixed(HUD_OBJETIVO_ITEM_X,HUD_OBJETIVO_ITEM_Y,PJ_SPRITE_W*3,PJ_SPRITE_H*3,hud->entidadObjetivo->sprite->getFrame(),true);
        renderComp->setClip(NULL);
    }
};

void Game::input(int tecla, bool estadoTecla){
    switch (tecla)
    {
    case SDL_SCANCODE_ESCAPE:
        if (estadoTecla) estado = TERMINADO;
        break;
    }
};
void Game::inputMouse(int tecla, int X, int Y)
{
    switch (tecla)
    {
    case SDL_BUTTON_LEFT:
        if(hud->getCursor()) hud->setCursorOff();
        else {
            hud->setCursor(X, Y);
            selectEntidades(X,Y);
        }
        break;
    }
};

void Game::selectEntidades(int X,int Y){
    /* Funcion que recibe una posicion y checkea si alguna entidad del juego la ocupa. y luego la pasa al HUD para setear objetivo
        Puede ser modificado para que devuelva la entidad y darle mayor scope a la funcion. */
    std::cout<<"Personajes: "<<personajes.size()<<"."<<std::endl<<"Objetos: "<<mapa->objetos.size()<<"."<<std::endl;
    for(int o = 0; o < personajes.size(); o++){
        if(personajes.at(o)->getX()/32 == X/32  && personajes.at(o)->getY()/32 == Y/32) hud->setObjetivo(personajes.at(o));
    }
    for(int o = 0; o < mapa->objetos.size(); o++){
        if(mapa->objetos.at(o)->getX() == X/32  && mapa->objetos.at(o)->getY() == Y/32) hud->setObjetivo(mapa->objetos.at(o));
    }
};

void Game::bindearInput(){
    in->signalTecla.connect(boost::bind(&Game::input, this, _1, _2));
    in->signalBoton.connect(boost::bind(&Game::inputMouse, this, _1, _2, _3));
    in->signalTecla.connect(boost::bind(&PersonajeJ::input, pj, _1, _2));
    in->signalBoton.connect(boost::bind(&PersonajeJ::inputMouse, pj, _1, _2, _3));
    
};

//Crear clase resource manager:
/*
**ResourceManager: Controla la carga y descarga de recursos. 
*/